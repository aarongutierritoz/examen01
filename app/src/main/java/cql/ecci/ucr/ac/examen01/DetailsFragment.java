package cql.ecci.ucr.ac.examen01;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import android.content.Intent;
import android.net.Uri;

import android.widget.TextView;

import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 */
public class DetailsFragment extends Fragment  implements View.OnClickListener {
    private double Latitude;
    private  double Longitude;
    private  String name = null;

    public DetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_details, container, false);
        TextView detail = (TextView) view.findViewById(R.id.gameDetail);

        detail.setOnClickListener(this);

        //extraemos de los argumentos el objeto
        TableTop bundle = (TableTop) getArguments().getSerializable("game");

        setMapAttributes(bundle);

        if(bundle != null){
            detail.setText("Description: "+bundle.getDescription()+"\n\n"
                    + "Publisher: "+bundle.getPublisher()+"\n\n"
                    + "Year:"+bundle.getYear()+"\n\n"
                    + "No Players:"+bundle.getNoplayers()+"\n\n"
                    + "Ages:"+bundle.getAges()+"\n\n"
                    + "Playing time:"+bundle.getPtime()+"");
        }



        return view;

    }

    //asignamos la latitud y la longitud para el intent al mapa
    private void setMapAttributes(TableTop bundle) {
        this.Latitude = bundle.getLatitude();
        this.Longitude = bundle.getLongitude();
        this.name = bundle.getName();
    }

    //cuando se presiona el fragmento de detalle, se va al mapa
    @Override
    public void onClick(View v) {
        irMapa(this.Latitude,this.Longitude,this.name);

    }
    //accedemos al mapa mediante intent a la aplicacion del telefono
    private void irMapa(double latitude,double longitude, String name){
        // Intent para ver la localización en el mapa
        String uri = String.format(Locale.ENGLISH, "geo:%f,%f", latitude, longitude);
        String q = "?q="+ latitude + "," +
                longitude + "(" + name + ")";
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri + q));
        intent.setPackage("com.google.android.apps.maps");
        startActivity(intent);
    }
}
