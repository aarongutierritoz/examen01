package cql.ecci.ucr.ac.examen01;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import androidx.appcompat.app.AppCompatActivity;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Collections;
import java.util.List;
import java.util.SimpleTimeZone;

public class MainActivity extends FragmentActivity {
    ListView list;
    String[] itemname = new String[5];
    ;
    private List<TableTop> tableTopsList;
    private List<String> ids;



    Integer[] imgid={
            R.drawable.catan,
            R.drawable.monopoly,
            R.drawable.eldritch,
            R.drawable.mtg,
            R.drawable.hanabi
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //leemos la tabla para ver si los valores ya existen
        int size = readGames();

        if(size == 0){
            insertGames();
            readGames();
        }
        listNames();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CustomListAdapter adapter = new CustomListAdapter(this, itemname, imgid);
        list = (ListView) findViewById(R.id.list);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub


                TableTop tp = tableTopsList.get(position);
                DetailsFragment detailsFragment = new DetailsFragment();

                //enviamos el objeto como un serializable mediante un bundle
                Bundle bundle = new Bundle();
                bundle.putSerializable("game", tp);

                //ejecutamos el fragmento
                detailsFragment.setArguments(bundle);
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.detail,detailsFragment).commit();
            }
        });
    }

    private int insertGames() {
        //solo vamos a insertar si al consultar la tabla nos devuelve un objeto vacio

       TableTop tableTop = new TableTop("TT001",
               "Catan",
               "1995",
               "Kosmos",
               "Germany",
               48.774538,
               9.188467,
               "Picture yourself in the era of discoveries: after a long voyage of great deprivation, your ships have finally reached the coast of an uncharted island. Its name shall be Catan! But you are not the only discoverer. Other fearless seafarers have also landed on the shores of Catan: the race to settle the island has begun!",
               "3-4",
               "10+","1-2 hours");

               long newRowId = tableTop.insertar(getApplicationContext());



        TableTop tableTo2 = new TableTop("TT002",
                "Monopoly",
                "1935",
                "Hasbro",
               " United States",
                41.883736,
                -71.352259,
                "The thrill of bankrupting an opponent, but it pays to play nice, because fortunes could change with the roll of the dice. Experience the ups and downs by collecting property colors sets to build houses, and maybe even upgrading to a hotel! The more properties each player owns, the more rent can be charged. Chance cards could be worth money, or one might just say Go To Jail!",
                "2-8",
                "8+",
                "20-180 minutes");

            long newRowId2 = tableTo2.insertar(getApplicationContext());



        TableTop tableTo3 = new TableTop("TT003",
                "Eldritch Horror",
                "2013",
                "Fantasy Flight Games",
                "United States",
                45.015417,
                -93.183995,
                "An ancient evil is stirring. You are part of a team of unlikely heroes engaged in an international struggle to stop the gathering darkness. To do so, you’ll have to defeat foul monsters, travel to Other Worlds, and solve obscure mysteries surrounding this unspeakable horror. The effort may drain your sanity and cripple your body, but if you fail, the Ancient One will awaken and rain doom upon the known world. ",
                "-8",
                "14+",
                "2-4 hours");
        long newRowId3 = tableTo3.insertar(getApplicationContext());

        TableTop tableTo4 = new TableTop("TT004",
                "Magic: the Gathering",
                "1993",
                "Hasbro",
                "United States",
                41.883736,
                -71.352259,
                "Magic: The Gathering is a collectible and digital collectible card game created by Richard Garfield. Each game of Magic represents a battle between wizards known as planeswalkers who cast spells, use artifacts, and summon creatures as depicted on individual cards in order to defeat their opponents, typically, but not always, by draining them of their 20 starting life points in the standard format.",
                "2+",
                "13+",
                "Varies");
        long newRowId4 = tableTo4.insertar(getApplicationContext());

        TableTop tableTo5 = new TableTop("TT005",
                "Hanabi",
                "2010",
                "Asmodee",
                "France",
                48.761629,
                2.065296,"Hanabi—named for the Japanese word for fireworks—is a cooperative game in which players try to create the perfect fireworks show by placing the cards on the table in the right order.",
                "2-5","" +
                "8+",
                "25 minutes");
        long newRowId45= tableTo5.insertar(getApplicationContext());
        return 0;
    }
    //Devuelve la tabla de juegos y se le asigna al atributo de clase
    private int readGames() {
        TableTop tp = new TableTop();
        tp.leer(getApplicationContext());
        this.tableTopsList = tp.fileName;
        return tableTopsList.size();

    }
    //Se insertan los nombres de los juegos en el ListView
    private void listNames(){
        int index = 0;
        while(index < tableTopsList.size()){
            itemname[index] = tableTopsList.get(index).getName();
            ++index;
        }
    }
}
