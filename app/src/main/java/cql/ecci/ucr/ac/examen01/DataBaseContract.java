package cql.ecci.ucr.ac.examen01;

import android.provider.BaseColumns;

public final class DataBaseContract {
    public static class DataBaseEntry implements BaseColumns {

        public static final String TABLE_NAME_TABLETOP = "TableTop";

        public static final String COLUMN_NAME_ID = "ID";
        public static final String COLUMN_NAME_NAME = "Name";
        public static final String COLUMN_NAME_YEAR = "Year";
        public static final String COLUMN_NAME_PUBLISHER = "Publisher";
        public static final String COLUMN_NAME_COUNTRY = "Country";
        public static final String COLUMN_NAME_LATITUDE = "Latitude";
        public static final String COLUMN_NAME_LONGITUDE = "Longitude";
        public static final String COLUMN_NAME_DESC = "Description";
        public static final String COLUMN_NAME_NOPLAYERS = "NoPlayers";
        public static final String COLUMN_NAME_AGES = "Ages";
        public static final String COLUMN_NAME_PTIME = "PlayingTime";

    }
    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String REAL_TYPE = " REAL";
    private static final String COMMA_SEP = ",";

    public static final String SQL_CREATE_TABLETOP = "CREATE TABLE " + DataBaseEntry.TABLE_NAME_TABLETOP + "("+
            DataBaseEntry._ID + TEXT_TYPE + " PRIMARY KEY," +
            DataBaseEntry.COLUMN_NAME_NAME + TEXT_TYPE + COMMA_SEP +
            DataBaseEntry.COLUMN_NAME_YEAR + TEXT_TYPE + COMMA_SEP +
            DataBaseEntry.COLUMN_NAME_PUBLISHER + TEXT_TYPE + COMMA_SEP +
            DataBaseEntry.COLUMN_NAME_COUNTRY + TEXT_TYPE + COMMA_SEP +
            DataBaseEntry.COLUMN_NAME_LATITUDE + REAL_TYPE + COMMA_SEP +
            DataBaseEntry.COLUMN_NAME_LONGITUDE + REAL_TYPE + COMMA_SEP +
            DataBaseEntry.COLUMN_NAME_DESC + TEXT_TYPE + COMMA_SEP +
            DataBaseEntry.COLUMN_NAME_NOPLAYERS + TEXT_TYPE + COMMA_SEP +
            DataBaseEntry.COLUMN_NAME_AGES + TEXT_TYPE + COMMA_SEP +
            DataBaseEntry.COLUMN_NAME_PTIME + TEXT_TYPE + ")";

    public static final String SQL_DELETE_TABLE_TOP =
            "DROP TABLE IF EXISTS " + DataBaseEntry.TABLE_NAME_TABLETOP;
}
