package cql.ecci.ucr.ac.examen01;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TableTop implements Serializable {
    List<TableTop> fileName = new ArrayList<>();

    private String  ID;
    private String name;
    private String year;
    private String publisher;
    private String country;
    private double latitude;
    private double longitude;
    private String description;
    private String noplayers;
    private String ages;
    private String ptime;

    public TableTop(){}

    public TableTop(String id,String name,String year,String publisher,String country, double latitude, double longitude,
                    String description, String noplayers,String ages, String ptime){
        setID(id);
        setName(name);
        setYear(year);
        setPublisher(publisher);
        setCountry(country);
        setLatitude(latitude);
        setLongitude(longitude);
        setDescription(description);
        setNoplayers(noplayers);
        setAges(ages);
        setPtime(ptime);

    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double logitude) {
        this.longitude = logitude;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNoplayers() {
        return noplayers;
    }

    public void setNoplayers(String noplayers) {
        this.noplayers = noplayers;
    }

    public String getAges() {
        return ages;
    }

    public void setAges(String ages) {
        this.ages = ages;
    }

    public String getPtime() {
        return ptime;
    }

    public void setPtime(String ptime) {
        this.ptime = ptime;
    }

    public long insertar(Context context) {
        // usar la clase DataBaseHelper para realizar la operacion de insertar
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        // Obtiene la base de datos en modo escritura
        SQLiteDatabase db = dataBaseHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DataBaseContract.DataBaseEntry._ID, getID());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_NAME,getName());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_YEAR,getYear());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_PUBLISHER,getPublisher());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_COUNTRY,getCountry());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_LATITUDE,getLatitude());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_LONGITUDE,getLongitude());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_DESC,getDescription());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_NOPLAYERS,getNoplayers());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_AGES,getAges());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_PTIME,getPtime());

        return db.insert(DataBaseContract.DataBaseEntry.TABLE_NAME_TABLETOP, null,
                values);
    }

    public void leer (Context context){
        // usar la clase DataBaseHelper para realizar la operacion de leer
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        // Obtiene la base de datos en modo lectura
        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
        // Define cuales columnas quiere solicitar // en este caso todas las de la clase
        String[] projection = {
                DataBaseContract.DataBaseEntry._ID,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_NAME,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_YEAR,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_PUBLISHER,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_COUNTRY,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_LATITUDE,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_LONGITUDE,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_DESC,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_NOPLAYERS,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_AGES,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_PTIME
        };

        // Resultados en el cursor
        Cursor cursor = db.query(
                DataBaseContract.DataBaseEntry.TABLE_NAME_TABLETOP, // tabla
                projection, // columnas
                null, // where
                null, // valores delwhere
                null, // agrupamiento
                null, // filtros po grupo
                null // orden
        );
        // recorrer los resultados y asignarlos a la clase // aca podriaimplementarse un ciclo si es necesario
        if(cursor.moveToFirst() && cursor.getCount() > 0) {
            while (!cursor.isAfterLast()) {
                TableTop x = new TableTop();
                x.setID(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry._ID)));
                x.setName(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_NAME)));
                x.setYear(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_YEAR)));
                x.setPublisher(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_PUBLISHER)));
                x.setCountry(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_COUNTRY)));
                x.setLatitude(cursor.getDouble(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_LATITUDE)));
                x.setLongitude(cursor.getDouble(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_LONGITUDE)));
                x.setDescription(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_DESC)));
                x.setNoplayers(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_NOPLAYERS)));
                x.setAges(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_AGES)));
                x.setPtime(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_PTIME)));

                fileName.add(x);
                cursor.moveToNext();
            }

        }
    }
    public void eliminarTableTop(Context context)
    {
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        SQLiteDatabase db = dataBaseHelper.getWritableDatabase();
        db.delete(DataBaseContract.DataBaseEntry.TABLE_NAME_TABLETOP, null, null);
    }

    @Override
    public  String toString(){

        return this.getName();
    }
}
