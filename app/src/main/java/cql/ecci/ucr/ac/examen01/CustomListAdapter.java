package cql.ecci.ucr.ac.examen01;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
public class CustomListAdapter extends ArrayAdapter<String> {
    private final Activity context;
    private final String[] itemname;
    private final Integer[] imgid;
    public CustomListAdapter(Activity context, String[] itemname, Integer[] imgid ) {
        super(context, R.layout.lista_iconos, itemname);
        this.context = context;
        this.itemname = itemname;
        this.imgid = imgid;

    }
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.lista_iconos, null, true);
        TextView nombre = (TextView) rowView.findViewById(R.id.name);
        TextView descripcion = (TextView) rowView.findViewById(R.id.description);

        ImageView imagen = (ImageView) rowView.findViewById(R.id.icon);
        nombre.setText(itemname[position]);
        imagen.setImageResource(imgid[position]);
        descripcion.setText(" ");;

        return rowView;
    }
}
